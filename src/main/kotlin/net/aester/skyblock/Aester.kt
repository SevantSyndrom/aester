package net.aester.skyblock

class Aester : JavaPlugin() {

    private lateinit var console: ConsoleCommandSender

    private lateinit var moduleManager: ModuleManager


    override fun onEnable() {

        console = Bukkit.getServer().consoleSender
        moduleManager = ModuleManager()

        console.sendMessage("""
            =============================
            Now enabling Aester ${this.description.version}
            =============================
        """.trimIndent())

        moduleManager.addModule(CoreModule())
    }

    override fun onDisable() {

        //console.sendMessage("""
        //    =============================
        //    Now disabling Aester ${this.description.version}
        //    =============================
        //""".trimIndent())
    }
}