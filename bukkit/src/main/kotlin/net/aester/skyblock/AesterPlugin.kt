package net.aester.skyblock

import co.aikar.commands.PaperCommandManager
import net.aester.skyblock.api.config.ConfigManager
import net.aester.skyblock.api.module.ModuleManager
import net.aester.skyblock.api.user.UserManager
import net.aester.skyblock.modules.core.CoreModule
import net.aester.skyblock.modules.core.commands.island.AesterCommand
import net.aester.skyblock.modules.core.commands.island.IslandCommand
import org.bukkit.Bukkit
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.plugin.java.JavaPlugin

class AesterPlugin : JavaPlugin() {


    private lateinit var console: ConsoleCommandSender

    private lateinit var moduleManager: ModuleManager

    lateinit var configManager: ConfigManager

    lateinit var userManager: UserManager

    lateinit var commandManager: PaperCommandManager



    override fun onEnable() {

        console = Bukkit.getServer().consoleSender
        moduleManager = ModuleManager()
        configManager = ConfigManager(this)
        userManager = UserManager(this)
        commandManager = PaperCommandManager(this)
        AesterPlugin.instance = this

        console.sendMessage("""
            =============================
            Now enabling Aester ${this.description.version}
            =============================
        """)

        moduleManager.addModule(CoreModule(this))
        registerCommands()



    }

    override fun onDisable() {

        console.sendMessage("""
            =============================
            Now disabling Aester ${this.description.version}
            =============================
        """)
    }

    private fun registerCommands() {
        commandManager.registerCommand(IslandCommand(this))
        commandManager.registerCommand(AesterCommand())
    }

    companion object {
        lateinit var instance: AesterPlugin


    }




}