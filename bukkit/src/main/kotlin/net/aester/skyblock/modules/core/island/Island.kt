package net.aester.skyblock.modules.core.island

import net.aester.skyblock.AesterPlugin
import ninja.leaping.configurate.hocon.HoconConfigurationLoader
import org.bukkit.OfflinePlayer
import java.io.File
import java.util.*

class Island(plugin: AesterPlugin, player: OfflinePlayer, uuid: UUID) {


    private val uuid = uuid
    private val ownerUUID = player.uniqueId
    private var level = IslandValue(uuid, plugin)
    private var size = 0
    private var deleted = false


    init {
        var islandData = HoconConfigurationLoader.builder().setFile(File(plugin.dataFolder.resolve("data"), "$uuid.data")).build().load()
        //var islandData = HoconConfiguration.loadConfiguration(plugin.dataFolder.resolve("data").resolve("$uuid.conf"))
        size = islandData.getNode("size").int


    }
}