package net.aester.skyblock.modules.core.commands.island

import co.aikar.commands.BaseCommand
import co.aikar.commands.CommandIssuer
import co.aikar.commands.annotation.CommandAlias
import co.aikar.commands.annotation.Subcommand
import net.aester.skyblock.AesterPlugin
import net.aester.skyblock.api.schematic.Selection


@CommandAlias("aester")
class AesterCommand : BaseCommand() {

    @Subcommand("posa")
    fun onPosA(commandIssuer: CommandIssuer) {
        if (commandIssuer.isPlayer) {
            val user = AesterPlugin.instance.userManager.getUser(commandIssuer.uniqueId)
            var selection: Selection = user!!.getAttrib("schematic-selection") as Selection? ?: Selection()
            selection.posA = user.getPlayer().location
            user.attributeMap["schematic-selection"] = selection
            commandIssuer.sendMessage("Set position A to ${selection.posA}")
        }
    }

    @Subcommand("posb")
    fun onPosB(commandIssuer: CommandIssuer) {
        if (commandIssuer.isPlayer) {
            val user = AesterPlugin.instance.userManager.getUser(commandIssuer.uniqueId)
            var selection: Selection = user!!.getAttrib("schematic-selection") as Selection? ?: Selection()
            selection.posB = user.getPlayer().location
            user.attributeMap["schematic-selection"] = selection
            commandIssuer.sendMessage("Set position B to ${selection.posB}")

        }
    }

    @Subcommand("saveschem")
    fun onSaveSchematic(commandIssuer: CommandIssuer, name: String) {
        if (commandIssuer.isPlayer) {
            val user = AesterPlugin.instance.userManager.getUser(commandIssuer.uniqueId)
            var selection: Selection = user!!.getAttrib("schematic-selection") as Selection
            selection.saveAsSchematicAsync(name)
        }
    }
}