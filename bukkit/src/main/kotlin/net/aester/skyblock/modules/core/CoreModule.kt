package net.aester.skyblock.modules.core

import net.aester.skyblock.AesterPlugin
import net.aester.skyblock.api.module.Module
import net.aester.skyblock.api.world.WorldManager
import net.aester.skyblock.modules.core.listeners.PlayerJoinListener

class CoreModule(val plugin: AesterPlugin) : Module("aester.core", false) {


    private val worldManager = WorldManager(plugin)

    override fun onEnable() {
        plugin.server.pluginManager.registerEvents(PlayerJoinListener(plugin.userManager), plugin)
    }

    override fun onDisable() {
    }
}