package net.aester.skyblock.modules.core.listeners

import net.aester.skyblock.AesterPlugin
import net.aester.skyblock.api.user.AesterUser
import net.aester.skyblock.api.user.UserManager
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent

class PlayerJoinListener(userManager: UserManager) : Listener {

    val userManager = userManager

    @EventHandler
    fun onPlayerJoin(event: PlayerJoinEvent) {
        println("registering user(${event.player.uniqueId}!")
        AesterPlugin.instance.userManager.registerUser(AesterUser(event.player.uniqueId))
    }
}