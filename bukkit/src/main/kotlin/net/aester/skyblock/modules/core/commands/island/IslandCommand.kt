package net.aester.skyblock.modules.core.commands.island

import co.aikar.commands.BaseCommand
import co.aikar.commands.annotation.CommandAlias
import co.aikar.commands.annotation.Subcommand
import net.aester.skyblock.AesterPlugin
import org.bukkit.entity.Player

@CommandAlias("is")
class IslandCommand(plugin: AesterPlugin) : BaseCommand() {

    val plugin = plugin

    @Subcommand("create")
    fun onCreate(player: Player) {
        if (plugin.userManager.getUser(player.uniqueId)!!.getAttrib("aester.island") == null) {

        }
    }
}