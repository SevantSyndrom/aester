package net.aester.skyblock.modules.core.island

import net.aester.skyblock.AesterPlugin
import net.aester.skyblock.api.config.KormConfiguration
import java.util.*


class IslandValue(island: UUID, plugin: AesterPlugin) {
    private val plugin = plugin
    private lateinit var owner: UUID
    private val island = island

    private var lastCalculatedValue = 0

    private val materials = hashMapOf<String, Int>()

    init {
        val config = KormConfiguration.loadConfiguration(plugin.dataFolder.resolve("data").resolve("island").resolve("$island.korm"))
        owner = UUID.fromString(config.getString("Owner"))

        for (s in config.getConfigurationSection("Value.Blocks").getKeys(false)) {
            materials[s] = config.getInt("Value.Blocks.$s.Amount")
        }
    }

    fun setOwnerUUID(owner: UUID) {
        this.owner = owner
    }

    fun getTotalValue(): Int {
        val config = plugin.configManager.getConfig("values")

        var total = 0

        for (s in materials.keys) {
            val amount = materials.getOrDefault(s, 0)
            total += config.getNode(s).int
        }
        return total
    }

    fun getValueOfMaterial(material: String): Int {
        val config = plugin.configManager.getConfig("values")

        var value = 0


        if (materials.containsKey(material)) {
            value = config.getNode(material).int * (materials[material] ?: 0)
        }

        return value
    }
}