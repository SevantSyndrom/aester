package net.aester.skyblock.api.world

import net.aester.skyblock.modules.core.island.Island
import org.bukkit.Bukkit
import org.bukkit.World
import org.bukkit.WorldCreator
import java.util.*

class World(world: World) {

    var world = world

    var islands = hashMapOf<UUID, Island>()


    companion object {
        fun of(name: String): net.aester.skyblock.api.world.World {
            return World(Bukkit.getWorld(name)
                    ?: createWorld(name))


        }

        private fun createWorld(name: String): World {
            return WorldCreator.name(name)
                    .type(org.bukkit.WorldType.FLAT)
                    .environment(World.Environment.NORMAL)
                    .generator(EmptyGenerator()).createWorld()
        }
    }

    enum class WorldType {
        OVERWORLD,
        NETHER,
        END
    }

}