package net.aester.skyblock.api.schematic

import com.google.gson.Gson

class EntityAttributeMap : AttributeMap<EntityAttributeMap.AttributeType, EntityDataAttribute<*>>() {

    fun get(key: AttributeType): EntityDataAttribute<*>? {
        return map[key]
    }

    operator fun set(key: AttributeType, value: EntityDataAttribute<*>) {
        map[key] = value
    }

    fun toJson(): String {
        return Gson().toJson(map)
    }

    fun fromJson(json: String) {
        Gson().fromJson(json, Map::class.java).forEach {
            map[AttributeType.valueOf(it.key.toString())] = EntityDataAttribute(it.value)
        }
    }


    enum class AttributeType {
        ENTITY_TYPE,
        HAND,
        HELMET,
        CHESTPLATE,
        LEGGINGS,
        BOOTS,
        BODYPOSE,
        HEADPOSE,
        LEFTARMPOSE,
        LEFTLEGPOSE,
        RIGHTARMPOSE,
        RIGHTLEGPOSE,
        OFFHAND,
        WOODTYPE,
        CARRYBLOCK,
        CUSTOMNAME,
        HORSECOLOR,
        HORSESTYLE,
        ITEM,
        ROTATE,
        LLAMACOLOR,
        OCELOTTYPE,
        ART,
        PARRTOVARIANT,
        RABBITTYPE,
        PROFESSION,
        COLOR,
        INVENTORY,
        POS_X,
        POS_Y,
        POS_Z,
        HANDCHANCE,
        OFFHANDCHANCE,
        HELMETCHANCE,
        CHESTPLATECHANCE,
        LEGGINGSCHANCE,
        BOOTSCHANCE,
        YAW,
        PITCH,
        VERSION,
        FIRETICKS,
        TICKSLIVED,
        LLAMASTRENGTH,
        ANGERLEVEL,
        SLIMESIZE,
        RICHES,
        AGE,
        ARMS,
        BASEPLATE,
        VISABLE,
        SMALL,
        MARKER,
        AWAKE,
        POWERED,
        CUSTOMNAMEVISABLE,
        CREATEDBYPLAYER,
        SADDLE,
        ANGRY,
        SHEARED,
        DERP,
        AGELOCK,
        BREED,
        AI,
        BABY
    }
}