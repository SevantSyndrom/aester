package net.aester.skyblock.api.nbt.nbtutils

import com.google.gson.Gson

class GsonWrapper {

    companion object {
        private val gson = Gson()

        fun getString(o: Any): String {
            return gson.toJson(o)
        }

        fun <T> deserializeJson(json: String, type: Class<T>): T? {
            try {
                if (json == null) return null

                val o = gson.fromJson(json, type)
                return type.cast(o)

            } catch (ex: Exception) {
                ex.printStackTrace()
                return null
            }
        }
    }
}