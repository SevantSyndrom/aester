package net.aester.skyblock.api.schematic

import com.google.gson.Gson

data class EntityStorage(val data: List<EntityData>) {

    companion object {
        fun fromString(data: String): EntityStorage {
            return Gson().fromJson(data, EntityStorage::class.java)
        }
    }
}