package net.aester.skyblock.api.nbt

import org.bukkit.Bukkit

enum class ClassWrapper(val prefix: String, val suffix: String) {

    CRAFT_ITEMSTACK("org.bukkit.craftbukkit.", ".inventory.CraftItemStack"),
    CRAFT_ENTITY("org.bukkit.craftbukkit.", "entity.CraftEntity"),
    CRAFT_WORLD("org.bukkit.craftbukkit.", ".CraftWorld"),
    NMS_NBTBASE("net.minecraft.server.", ".NBTBase"),
    NMS_NBTTAGSTRING("net.minecraft.server.", ".NBTTagString"),
    NMS_ITEMSTACK("net.minecraft.server.", ".ItemStack"),
    NMS_NBTTAGCOMPOUND("net.minecraft.server.", ".NBTTagCompound"),
    NMS_NBTTAGLIST("net.minecraft.server.", ".NBTTagList"),
    NMS_NBTCOMPRESSEDSTREAMTOOLS("net.minecraft.server.", ".NBTCompressedStreamTools"),
    NMS_MOJANGGSONPARSER("net.minecraft.server.", ".MojangsonParser"),
    NMS_TILEENTITY("net.minecraft.server.", ".TileEntity"),
    NMS_BLOCKPOSITION("net.minecraft.server.", ".BlockPosition"),
    NMS_WORLD("net.minecraft.server.", ".WorldServer"),
    NMS_ENTITY("net.minecraft.server.", ".Entity");

    var clazz: Class<*>? = null


    init {

        try {

            val version = Bukkit.getServer().javaClass.`package`.name.replace(".", ",").split(",")[3]
            clazz = Class.forName("$prefix$version$suffix")

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }


}