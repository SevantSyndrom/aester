package net.aester.skyblock.api.schematic

data class EntityDataAttribute<T>(val value: T)