package net.aester.skyblock.api.schematic

import com.google.gson.Gson
import org.bukkit.entity.Entity

data class EntityData(
        val type: String?,
        val hand: String?,
        val helmet: String?,
        val chestPlate: String?,
        val leggings: String?,
        val bodyPose: String?,
        val leftArmPose: String?,
        val rightArmPose: String?,
        val leftLegPose: String?,
        val rightLegPose: String?,
        val offHand: String?,
        val woodType: String?,
        val carryBlock: String?,
        val customName: String?,
        val horseColor: String?,
        val horseStyle: String?,
        val item: String?,
        val rotate: String?,
        val llamaColor: String?,
        val ocelotType: String?,
        val art: String?,
        val parrotVariant: String?,
        val rabbitType: String?,
        val profession: String?,
        val color: String?,

        val inventory: List<String>?,

        val x: Double?,
        val y: Double?,
        val z: Double?,

        val handChance: Float?,
        val offHandChance: Float?,
        val helmetChance: Float?,
        val chestplateChance: Float?,
        val leggingsChance: Float?,
        val bootsChance: Float?,
        val yaw: Float?,
        val pitch: Float?,

        val version: Int?,
        val fireTicks: Int?,
        val ticksLived: Int?,
        val llamaStrength: Int?,
        val angerLevel: Int?,
        val slimeSize: Int?,
        val riches: Int?,
        val age: Int?,

        val arms: Boolean?,
        val basePlate: Boolean?,
        val visable: Boolean?,
        val small: Boolean?,
        val marker: Boolean?,
        val awake: Boolean?,
        val powered: Boolean?,
        val customNameVisable: Boolean?,
        val createdByPlayer: Boolean?,
        val saddle: Boolean?,
        val angry: Boolean?,
        val sheared: Boolean?,
        val derp: Boolean?,
        val ageLock: Boolean?,
        val breed: Boolean?,
        val ai: Boolean?,
        val baby: Boolean?

) {
    companion object {
        fun fromString(data: String): EntityData {
            return Gson().fromJson(data, EntityData::class.java)
        }

        class builder(entity: Entity) {
            val location = entity.location
        }
    }
}