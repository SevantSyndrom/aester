package net.aester.skyblock.api.schematic

import com.google.gson.Gson
import net.aester.skyblock.AesterPlugin
import org.bukkit.Location
import org.bukkit.Material
import java.io.File
import java.io.FileWriter


class Selection {

    var posA: Location? = null
    var posB: Location? = null


    fun saveAsSchematicAsync(name: String) {

        if (posA == null || posB == null) {
            println("pos a = $posA pos b = $posB")
        } else {
            val zmin = Math.min(posA!!.z, posB!!.z)
            val zmax = Math.max(posA!!.z, posB!!.z)
            val xmin = Math.min(posA!!.x, posB!!.x)
            val xmax = Math.max(posA!!.x, posB!!.x)
            val ymin = Math.min(posA!!.y, posB!!.y)
            val ymax = Math.max(posA!!.y, posB!!.y)

            val blocks = mutableListOf<Block>()

            for (z in zmin.toInt()..zmax.toInt()) {

                for (y in ymin.toInt()..ymax.toInt()) {
                    for (x in xmin.toInt()..xmax.toInt()) {
                        val a = posA
                        val b = posB
                        val loc = Location(a!!.world, x.toDouble(), y.toDouble(), z.toDouble())
                        if (loc.block.type == Material.AIR) break
                        println("Adding block at (x:$x y:$y z:$z) as type: ${loc.block.type}")
                        blocks.add(Block(loc.block.state, loc))
                    }
                }
            }

            //Schematic.save(File(AesterPlugin.instance.dataFolder.resolve("schematics"), "$name.schem"), BlockStorage(blocks))
            val file = File(AesterPlugin.instance.dataFolder.resolve("schematics"), "$name.schem")
            val storage = BlockStorage(blocks)
            val gson = Gson()
            println("Block storage = $storage")
            println("Json string = ${gson.toJson(storage)}")
            gson.toJson(storage, FileWriter(file.path))
            //Gson().toJson(storage, FileWriter(file.path))
        }

    }

}