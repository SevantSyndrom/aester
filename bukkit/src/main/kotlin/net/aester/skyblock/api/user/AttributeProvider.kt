package net.aester.skyblock.api.user

import ninja.leaping.configurate.ConfigurationNode

interface AttributeProvider {

    fun onRegiser(user: AesterUser, data: ConfigurationNode)
}