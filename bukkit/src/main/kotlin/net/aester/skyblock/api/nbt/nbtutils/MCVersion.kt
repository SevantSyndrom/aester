package net.aester.skyblock.api.nbt.nbtutils

import org.bukkit.Bukkit

enum class MCVersion(val versionID: Int) {
    UNKNOWN(Int.MAX_VALUE),
    MC1_12_R1(1121),
    MC1_13_R1(1131),
    MC1_13_R2(1132);


    companion object {

        private var version: MCVersion? = null

        fun getVersion(): MCVersion {

            if (version != null) return version as MCVersion

            val ver = Bukkit.getServer().javaClass.`package`.name.split("\\.")[3]
            println("Using version $ver; attempting NMS support")
            try {
                version = MCVersion.valueOf(ver.replace("v", "MC"))
            } catch (ex: IllegalArgumentException) {
                version = MCVersion.UNKNOWN
            }

            if (version != UNKNOWN) println("NMS Support for $ver successfully hooked!")
            else println("Wasnt able to hook into nms support!")
            return version!!

        }


    }


}