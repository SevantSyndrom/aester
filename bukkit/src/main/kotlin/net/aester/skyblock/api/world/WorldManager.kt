package net.aester.skyblock.api.world

import net.aester.skyblock.AesterPlugin


class WorldManager(plugin: AesterPlugin) {

    var worlds = mutableListOf<World>()

    init {
        val conf = plugin.configManager.getConfig("aester.config").getNode("worlds")

        for (w in conf.childrenList) {
            worlds.add(World.of(w.getNode("name").getString("")))
        }


    }


}