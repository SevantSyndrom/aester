package net.aester.skyblock.api.config

import org.bukkit.configuration.file.FileConfigurationOptions

class KormConfigurationOptions(configuration : KormConfiguration) : FileConfigurationOptions(configuration) {


    override fun configuration(): KormConfiguration {
        return super.configuration() as KormConfiguration
    }

    override fun copyDefaults(value: Boolean): FileConfigurationOptions {
        super.copyDefaults(value)
        return this
    }

    override fun pathSeparator(seperator: Char): FileConfigurationOptions {
        super.pathSeparator(seperator)
        return this
    }




}