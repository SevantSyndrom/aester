package net.aester.skyblock.api.nbt

class NBTCompound(val compoundName: String, val parent: NBTCompound) {

    fun getCompound(): Any {
        return parent.getCompound()
    }

    protected fun setCompount(compound: Any) {
        parent.setCompount(compound)
    }


}