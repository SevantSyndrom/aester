package net.aester.skyblock.api.task

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

abstract class AsyncTask : Task() {


    private fun runAsync() = GlobalScope.async {
        run()
    }
}