package net.aester.skyblock.api.schematic

data class EquipmentContainer(
        val hand: String?,
        val helmet: String?,
        val chestPlate: String?,
        val leggings: String?,
        val bodyPose: String?
)