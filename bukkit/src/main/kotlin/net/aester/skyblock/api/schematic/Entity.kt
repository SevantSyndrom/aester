package net.aester.skyblock.api.schematic

import com.google.gson.Gson
import net.aester.skyblock.api.schematic.EntityAttributeMap.AttributeType
import org.bukkit.entity.*
import org.bukkit.entity.Entity
import org.bukkit.entity.minecart.HopperMinecart
import org.bukkit.entity.minecart.StorageMinecart
import org.bukkit.inventory.InventoryHolder

class Entity(entity: Entity) {

    val attributeMap = EntityAttributeMap()
    private val gson = Gson()

    init {
        when (entity.type) {
            EntityType.IRON_GOLEM -> attributeMap[AttributeType.CREATEDBYPLAYER] = EntityDataAttribute((entity as IronGolem).isPlayerCreated)
            EntityType.OCELOT -> attributeMap[AttributeType.OCELOTTYPE] = EntityDataAttribute((entity as Ocelot).catType.toString())
            EntityType.PIG -> attributeMap[AttributeType.SADDLE] = EntityDataAttribute((entity as Pig).hasSaddle())
            EntityType.ZOMBIE -> attributeMap[AttributeType.BABY] = EntityDataAttribute((entity as Zombie).isBaby)
            EntityType.BAT -> attributeMap[AttributeType.AWAKE] = EntityDataAttribute((entity as Bat).isAwake)
            EntityType.CREEPER -> attributeMap[AttributeType.POWERED] = EntityDataAttribute((entity as Creeper).isPowered)
            EntityType.RABBIT -> attributeMap[AttributeType.RABBITTYPE] = EntityDataAttribute((entity as Rabbit).rabbitType.toString())
            EntityType.SLIME -> attributeMap[AttributeType.SLIMESIZE] = EntityDataAttribute((entity as Slime).size)
            EntityType.SNOWMAN -> attributeMap[AttributeType.DERP] = EntityDataAttribute((entity as Snowman).isDerp)
            EntityType.ENDERMAN -> if ((entity as Enderman).carriedBlock != null) attributeMap[AttributeType.CARRYBLOCK] = EntityDataAttribute(entity.carriedBlock.material.toString())
            EntityType.PARROT -> attributeMap[AttributeType.PARRTOVARIANT] = EntityDataAttribute((entity as Parrot).variant.toString())
            EntityType.BOAT -> attributeMap[AttributeType.WOODTYPE] = EntityDataAttribute((entity as Boat).woodType.toString())
            EntityType.PAINTING -> attributeMap[AttributeType.ART] = EntityDataAttribute((entity as Painting).art.toString())
            EntityType.ARMOR_STAND -> {
                val e = entity as ArmorStand
                attributeMap[AttributeType.ARMS] = EntityDataAttribute(e.hasArms())
                attributeMap[AttributeType.HAND] = EntityDataAttribute(gson.toJson(e.itemInHand.serialize()))
                attributeMap[AttributeType.HELMET] = EntityDataAttribute(gson.toJson(e.helmet.serialize()))
                attributeMap[AttributeType.CHESTPLATE] = EntityDataAttribute(gson.toJson(e.chestplate.serialize()))
                attributeMap[AttributeType.LEGGINGS] = EntityDataAttribute(gson.toJson(e.leggings.serialize()))
                attributeMap[AttributeType.BOOTS] = EntityDataAttribute(gson.toJson(e.boots.serialize()))
                attributeMap[AttributeType.BASEPLATE] = EntityDataAttribute(e.hasBasePlate())
                attributeMap[AttributeType.VISABLE] = EntityDataAttribute(e.isVisible)
                attributeMap[AttributeType.SMALL] = EntityDataAttribute(e.isSmall)
                attributeMap[AttributeType.MARKER] = EntityDataAttribute(e.isMarker)
                attributeMap[AttributeType.BODYPOSE] = EntityDataAttribute("${e.bodyPose.x} ${e.bodyPose.y} ${e.bodyPose.z}")
                attributeMap[AttributeType.HEADPOSE] = EntityDataAttribute("${e.headPose.x} ${e.headPose.y} ${e.headPose.z}")
                attributeMap[AttributeType.LEFTARMPOSE] = EntityDataAttribute("${e.leftArmPose.x} ${e.leftArmPose.y} ${e.leftArmPose.z}")
                attributeMap[AttributeType.LEFTLEGPOSE] = EntityDataAttribute("${e.leftLegPose.x} ${e.leftLegPose.y} ${e.leftLegPose.z}")
                attributeMap[AttributeType.RIGHTARMPOSE] = EntityDataAttribute("${e.rightArmPose.x} ${e.rightArmPose.y} ${e.rightArmPose.z}")
                attributeMap[AttributeType.RIGHTLEGPOSE] = EntityDataAttribute("${e.rightLegPose.x} ${e.rightLegPose.y} ${e.rightLegPose.z}")
            }
            EntityType.HORSE -> {
                val e = entity as Horse
                val items = mutableListOf<String>()
                e.inventory.contents.forEach { items.add(gson.toJson(it.serialize())) }
                attributeMap[AttributeType.HORSECOLOR] = EntityDataAttribute(e.color.toString())
                attributeMap[AttributeType.HORSESTYLE] = EntityDataAttribute(e.style.toString())
                attributeMap[AttributeType.INVENTORY] = EntityDataAttribute(items)
            }
            EntityType.PIG_ZOMBIE -> {
                val e = entity as PigZombie
                attributeMap[AttributeType.ANGRY] = EntityDataAttribute(e.isAngry)
                attributeMap[AttributeType.ANGERLEVEL] = EntityDataAttribute(e.anger)
            }
            EntityType.SHEEP -> {
                val e = entity as Sheep
                attributeMap[AttributeType.SHEARED] = EntityDataAttribute(e.isSheared)
                attributeMap[AttributeType.COLOR] = EntityDataAttribute(e.color.toString())
            }
            EntityType.VILLAGER -> {
                val e = entity as Villager
                val items = mutableListOf<String>()
                e.inventory.contents.forEach { items.add(gson.toJson(it.serialize())) }
                attributeMap[AttributeType.PROFESSION] = EntityDataAttribute(e.profession.toString())
                attributeMap[AttributeType.RICHES] = EntityDataAttribute(e.riches)
                attributeMap[AttributeType.INVENTORY] = EntityDataAttribute(items)

            }
            EntityType.LLAMA -> {
                val e = entity as Llama
                val items = mutableListOf<String>()
                e.inventory.contents.forEach { items.add(gson.toJson(it.serialize())) }
                attributeMap[AttributeType.LLAMACOLOR] = EntityDataAttribute(e.color.toString())
                attributeMap[AttributeType.LLAMASTRENGTH] = EntityDataAttribute(e.strength)
                attributeMap[AttributeType.INVENTORY] = EntityDataAttribute(items)
            }
            EntityType.MINECART -> {
                if (entity is StorageMinecart || entity is HopperMinecart) {
                    val items = mutableListOf<String>()
                    (entity as InventoryHolder).inventory.contents.forEach { items.add(gson.toJson(it.serialize())) }
                    attributeMap[AttributeType.INVENTORY] = EntityDataAttribute(items)
                }
            }
            EntityType.ITEM_FRAME -> {
                var e = entity as ItemFrame
                var i = e.item
                if (i != null) attributeMap[AttributeType.ITEM] = EntityDataAttribute(gson.toJson(i.serialize()))
            }
        }
        if (entity is LivingEntity) {
            val equipment = entity.equipment
            attributeMap[AttributeType.AI] = EntityDataAttribute(entity.hasAI())
            attributeMap[AttributeType.HAND] = EntityDataAttribute(gson.toJson(equipment.itemInMainHand.serialize()))
            attributeMap[AttributeType.OFFHAND] = EntityDataAttribute(gson.toJson(equipment.itemInOffHand.serialize()))
            attributeMap[AttributeType.CHESTPLATE] = EntityDataAttribute(gson.toJson(equipment.chestplate.serialize()))
            attributeMap[AttributeType.HELMET] = EntityDataAttribute(gson.toJson(equipment.helmet.serialize()))
            attributeMap[AttributeType.LEGGINGS] = EntityDataAttribute(gson.toJson(equipment.leggings.serialize()))
            attributeMap[AttributeType.BOOTS] = EntityDataAttribute(gson.toJson(equipment.boots.serialize()))
            attributeMap[AttributeType.HANDCHANCE] = EntityDataAttribute(equipment.itemInMainHandDropChance)
            attributeMap[AttributeType.OFFHANDCHANCE] = EntityDataAttribute(equipment.itemInOffHandDropChance)
            attributeMap[AttributeType.HELMETCHANCE] = EntityDataAttribute(equipment.helmetDropChance)
            attributeMap[AttributeType.CHESTPLATECHANCE] = EntityDataAttribute(equipment.chestplateDropChance)
            attributeMap[AttributeType.LEGGINGSCHANCE] = EntityDataAttribute(equipment.leggingsDropChance)
            attributeMap[AttributeType.BOOTSCHANCE] = EntityDataAttribute(equipment.bootsDropChance)
        }
        if (entity is Ageable) {
            attributeMap[AttributeType.BREED] = EntityDataAttribute(entity.canBreed())
            attributeMap[AttributeType.AGE] = EntityDataAttribute(entity.age)
            attributeMap[AttributeType.AGELOCK] = EntityDataAttribute(entity.ageLock)
            attributeMap[AttributeType.BABY] = EntityDataAttribute(!entity.isAdult)
        }
    }
}