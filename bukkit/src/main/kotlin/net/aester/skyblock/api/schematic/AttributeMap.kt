package net.aester.skyblock.api.schematic

abstract class AttributeMap<K, V> {
    val map = mutableMapOf<K, V>()
}