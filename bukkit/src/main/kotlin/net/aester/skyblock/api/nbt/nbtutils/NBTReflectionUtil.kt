package net.aester.skyblock.api.nbt.nbtutils

import net.aester.skyblock.api.nbt.ClassWrapper
import net.aester.skyblock.api.nbt.NBTCompound
import org.bukkit.entity.Entity
import java.io.FileInputStream
import java.io.FileOutputStream
import java.lang.reflect.Constructor


class NBTReflectionUtil {

    companion object {


        fun getNMSEntity(entity: Entity): Any? {

            try {
                return ReflectionMethod.CRAFT_ENTITY_GET_HANDLE.run(ClassWrapper.CRAFT_ENTITY.clazz?.cast(entity))

            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            return null
        }

        fun readNBTFile(stream: FileInputStream): Any? {
            try {
                return ReflectionMethod.NBTFILE_READ.run(null, arrayOf(stream))
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            return null
        }

        fun saveNBTFile(nbt: Any, stream: FileOutputStream): Any? {
            try {
                return ReflectionMethod.NBTFILE_WRITE.run(null, arrayOf(nbt, stream))
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            return null
        }

        fun getItemRootNBTTagCompound(nmsitem: Any): Any? {
            val clazz = nmsitem::class.java
            try {
                val method = clazz.getMethod("getTag")
                val answer = method.invoke(nmsitem)
                return answer
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            return null
        }

        fun convertNBTCompoundtoNMSItem(nbtCompound: NBTCompound): Any? {
            val clazz = ClassWrapper.NMS_ITEMSTACK.clazz!!
            try {
                val constructor: Constructor<*> = clazz.getConstructor(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz)
                constructor.isAccessible = true
                return constructor.newInstance(nbtCompound.getCompound())
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            return null
        }
    }
}