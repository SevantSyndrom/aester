package net.aester.skyblock.api.user

import net.aester.skyblock.AesterPlugin
import net.aester.skyblock.modules.core.listeners.PlayerJoinListener
import ninja.leaping.configurate.ConfigurationNode
import ninja.leaping.configurate.hocon.HoconConfigurationLoader
import java.io.File
import java.util.*

class UserManager(plugin: AesterPlugin) {

    private val plugin = plugin

    private val users = hashMapOf<UUID, AesterUser>()

    private val attributeProviders = mutableListOf<AttributeProvider>()


    fun getUser(uuid: UUID): AesterUser? {
        return users[uuid]
    }


    fun registerUser(user: AesterUser) {

        attributeProviders.forEach { it.onRegiser(user, getUserDataObject(user)) }
        users[user.uuid] = user
    }

    fun deregisterUser(user: AesterUser) {
        users.remove(user.uuid)
    }

    fun getUserDataObject(user: AesterUser): ConfigurationNode {

        val file = File(plugin.dataFolder.resolve("data").resolve("users"), "${user.uuid}.data")
        if (!(file.exists())) {
            makeUserDataObject(user, file)
        }
        return HoconConfigurationLoader.builder().setFile(file).build().load()
    }

    fun makeUserDataObject(user: AesterUser, file: File) {
        val empty = HoconConfigurationLoader.builder().setPath(file.toPath()).build()
        val node = empty.createEmptyNode()
        node.getNode("uuid").value = "${user.uuid}"
        empty.save(node)


    }


    init {
        attributeProviders.add(IslandAttributeProvider())
        plugin.server.pluginManager.registerEvents(PlayerJoinListener(this), plugin)
    }
}