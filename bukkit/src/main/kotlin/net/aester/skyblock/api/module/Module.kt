package net.aester.skyblock.api.module

abstract class Module(identifier: String, canDisable: Boolean) {

    val identifier = identifier
    val canDisable = canDisable


    abstract fun onEnable()

    abstract fun onDisable()


}