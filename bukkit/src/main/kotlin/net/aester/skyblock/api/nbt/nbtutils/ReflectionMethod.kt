package net.aester.skyblock.api.nbt.nbtutils

import net.aester.skyblock.api.nbt.ClassWrapper
import org.bukkit.inventory.ItemStack
import java.io.InputStream
import java.io.OutputStream
import java.lang.reflect.Method

enum class ReflectionMethod(val target: Class<*>, val args: Array<Class<*>>, val existedSince: MCVersion, val names: Map<MCVersion, String>) {

    COMPOUND_SET_FLOAT(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java, Float::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "setFloat")),
    COMPOUND_SET_STRING(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java, String::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "setString")),
    COMPOUND_SET_INT(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java, Int::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "setInt")),
    COMPOUND_SET_BYTEARRAY(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java, byteArrayOf()::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "setByteArray")),
    COMPOUND_SET_INTARRAY(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java, intArrayOf()::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "setIntArray")),
    COMPOUND_SET_LONG(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java, Long::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "setLong")),
    COMPOUND_SET_SHORT(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java, Short::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "setShort")),
    COMPOUND_SET_BYTE(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java, Byte::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "setByte")),
    COMPOUND_SET_DOUBLE(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java, Double::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "setDouble")),
    COMPOUND_SET_BOOLEAN(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java, Boolean::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "setBoolean")),
    COMPOUND_ADD(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "a")),

    COMPOUND_GET_FLOAT(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "getFloat")),
    COMPOUND_GET_STRING(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "getString")),
    COMPOUND_GET_INT(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "getINT")),
    COMPUND_GET_BYTEARRAY(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "getByteArray")),
    COMPOUND_GET_INTARRAY(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "getIntArray")),
    COMPOUND_GET_LONG(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "getLong")),
    COMPOUND_GET_SHORT(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "getShort")),
    COMPOUND_GET_BYTE(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "getByte")),
    COMPOUND_GET_DOUBLE(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "getDouble")),
    COMPOUND_GET_BOOLEAN(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "getBoolean")),

    COMPOUND_REMOVE_KEY(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "remove")),
    COMPOUND_HAS_KEY(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "hasKeys")),
    COMPOUND_GET_TYPE(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(String::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "d")),
    COMPOUND_GET_KEYS(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "c", MCVersion.MC1_13_R1 to "getKeys")),

    LISTCOMPOUND_GET_KEYS(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, arrayOf(), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "c", MCVersion.MC1_13_R1 to "getKeys")),
    LIST_REMOVE_KEY(ClassWrapper.NMS_NBTTAGLIST.clazz!!, arrayOf(Int::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "remove")),
    LIST_SIZE(ClassWrapper.NMS_NBTTAGLIST.clazz!!, arrayOf(), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "size")),
    LIST_SET(ClassWrapper.NMS_NBTTAGLIST.clazz!!, arrayOf(Int::class.java, ClassWrapper.NMS_NBTBASE.clazz!!), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "a", MCVersion.MC1_13_R1 to "set")),
    LIST_ADD(ClassWrapper.NMS_NBTTAGLIST.clazz!!, arrayOf(ClassWrapper.NMS_NBTBASE.clazz!!), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "add")),
    LIST_GET_STRING(ClassWrapper.NMS_NBTTAGLIST.clazz!!, arrayOf(Int::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "getString")),
    LIST_GET(ClassWrapper.NMS_NBTTAGLIST.clazz!!, arrayOf(Int::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "get")),

    ITEMSTACK_SET_TAG(ClassWrapper.NMS_ITEMSTACK.clazz!!, arrayOf(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "setTag")),
    ITEMSTACK_NMSCOPY(ClassWrapper.CRAFT_ITEMSTACK.clazz!!, arrayOf(ItemStack::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "asNMSCopy")),
    ITEMSTACK_BUKKITMIRROR(ClassWrapper.CRAFT_ITEMSTACK.clazz!!, arrayOf(ClassWrapper.NMS_ITEMSTACK.clazz!!), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "asCraftMirror")),

    CRAFT_WORLD_GET_HANDLE(ClassWrapper.CRAFT_WORLD.clazz!!, arrayOf(), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "getHandle")),
    NMS_WORLD_GET_TILEENTITY(ClassWrapper.NMS_WORLD.clazz!!, arrayOf(ClassWrapper.NMS_BLOCKPOSITION.clazz!!), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "getTileEntity")),

    TILEENTITY_GET_NBT(ClassWrapper.NMS_TILEENTITY.clazz!!, arrayOf(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "save")),
    TILEENTITY_SET_NBT(ClassWrapper.NMS_TILEENTITY.clazz!!, arrayOf(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "load")),
    CRAFT_ENTITY_GET_HANDLE(ClassWrapper.CRAFT_ENTITY.clazz!!, arrayOf(), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "getHandle")),
    NMS_ENTITY_SET_NBT(ClassWrapper.NMS_ENTITY.clazz!!, arrayOf(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "f")),
    NMS_ENTITY_GET_NBT(ClassWrapper.NMS_ENTITY.clazz!!, arrayOf(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "save")),

    NBTFILE_READ(ClassWrapper.NMS_NBTCOMPRESSEDSTREAMTOOLS.clazz!!, arrayOf(InputStream::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "a")),
    NBTFILE_WRITE(ClassWrapper.NMS_NBTCOMPRESSEDSTREAMTOOLS.clazz!!, arrayOf(ClassWrapper.NMS_NBTTAGCOMPOUND.clazz!!, OutputStream::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "a")),

    PARSE_NBT(ClassWrapper.NMS_MOJANGGSONPARSER.clazz!!, arrayOf(String::class.java), MCVersion.MC1_12_R1, mapOf(MCVersion.MC1_12_R1 to "parse"));


    private lateinit var targetVersion: Pair<MCVersion, String>
    private lateinit var method: Method
    private var loaded = false
    private var compatible = false

    init {
        val server = MCVersion.getVersion()
        if (server.compareTo(existedSince) < 0) {

        } else {
            compatible = true
            lateinit var t: Pair<MCVersion, String>
            for (s in names) {
                if (t == null) {
                    t = Pair(s.key, s.value)
                }
                if (s.key.versionID <= server.versionID && t.first.versionID < s.key.versionID) {
                    t = Pair(s.key, s.value)
                }
            }
            targetVersion = t
            try {
                method = target::class.java.getMethod(targetVersion.second, *args)
                method.isAccessible = true
                loaded = true
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            } catch (ex: NoSuchMethodException) {
                ex.printStackTrace()
            } catch (ex: SecurityException) {
                ex.printStackTrace()
            }
        }
    }

    fun run(target: Any?, args: Array<*>): Any? {
        try {
            return method.invoke(target, *args)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return null
    }

}