package net.aester.skyblock.api.module

class ModuleManager {

    private val modules = mutableMapOf<String, Module>()

    fun addModule(module: Module) {
        modules.putIfAbsent(module.identifier, module)
        module.onEnable()
    }

    fun getModule(identifier: String): Module? {
        return modules[identifier]
    }

    fun disableModule(idendifier: String) {
        modules[idendifier]?.onDisable()
        modules.remove(idendifier)
    }
}