package net.aester.skyblock.api.config

import com.sxtanna.korm.Korm
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.configuration.file.FileConfigurationOptions
import java.io.File


class KormConfiguration : FileConfiguration() {



    override fun buildHeader(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun saveToString(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    private val COMMENT_PREFIX = "//"
    private val EMPTY_CONFIG = "{}\n"
    private val korm = Korm()





    private fun convertMapsToSection(input : Map<Any, Any>, section: ConfigurationSection) {
        for((k,v) in input){
            var key = k.toString()
            var value = v

            if(value is Map<*, *>){
                convertMapsToSection(value as Map<Any, Any>, section)
            } else {
                section.set(key, value)
            }
        }
    }

    override fun loadFromString(contents: String) {
        //val input : Map<Any, Any>?  = korm.pull(contents).mapData<Any>()
        val input = mapOf<Any, Any>()

        if(input != null){
            convertMapsToSection(input, this)
        }


    }




    override fun options(): FileConfigurationOptions {
        return options as FileConfigurationOptions? ?: KormConfigurationOptions(this)
    }

    companion object {
        fun loadConfiguration(file: File): KormConfiguration {

            val config = KormConfiguration()

            config.load(file)
            return config

        }
    }
}