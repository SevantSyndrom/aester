package net.aester.skyblock.api.schematic

import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import java.io.File
import java.io.FileReader
import java.io.FileWriter

class Schematic(file: File) {

    val blockStorage: BlockStorage = Gson().fromJson(JsonReader(FileReader(file)), BlockStorage::class.java)


    companion object {

        fun save(file: File, storage: BlockStorage) {
            val gson = Gson()
            println("Json string = ${gson.toJson(storage)}")
            Gson().toJson(storage, FileWriter(file.path))

        }
    }

}