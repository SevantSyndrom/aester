package net.aester.skyblock.api.user

import net.aester.skyblock.AesterPlugin
import net.aester.skyblock.modules.core.island.Island
import ninja.leaping.configurate.ConfigurationNode
import org.bukkit.Bukkit
import java.util.*

class IslandAttributeProvider : AttributeProvider {


    override fun onRegiser(user: AesterUser, data: ConfigurationNode) {

        val data = AesterPlugin.instance.userManager.getUserDataObject(user)

        val islandData = data.getNode("island")
        if (islandData.hasListChildren()) {
            val islandID = UUID.fromString(islandData.getString("id"))
            val offlinePlayer = Bukkit.getOfflinePlayer(user.uuid)

            user.attributeMap["aester.island"] = Island(AesterPlugin.instance, offlinePlayer, islandID)

        }
    }
}