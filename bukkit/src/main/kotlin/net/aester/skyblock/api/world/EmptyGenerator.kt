package net.aester.skyblock.api.world

import org.bukkit.World
import org.bukkit.generator.BlockPopulator
import org.bukkit.generator.ChunkGenerator
import java.util.*

class EmptyGenerator : ChunkGenerator() {

    override fun generateChunkData(world: World?, random: Random?, x: Int, z: Int, biome: BiomeGrid?): ChunkData {
        return createChunkData(world)
    }

    override fun getDefaultPopulators(world: World?): MutableList<BlockPopulator> {
        return mutableListOf()
    }

    override fun canSpawn(world: World?, x: Int, z: Int): Boolean {
        return true
    }
}