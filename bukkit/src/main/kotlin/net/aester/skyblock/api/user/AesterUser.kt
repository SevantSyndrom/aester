package net.aester.skyblock.api.user


import org.bukkit.Bukkit
import org.bukkit.entity.Player
import java.util.*

class AesterUser(uuid: UUID) {


    val uuid = uuid

    var attributeMap = hashMapOf<String, Any>()

    fun getAttrib(name: String): Any? {
        return attributeMap[name]
    }

    fun getPlayer(): Player {
        return Bukkit.getPlayer(uuid)
    }
}