package net.aester.skyblock.api.config

import net.aester.skyblock.AesterPlugin
import ninja.leaping.configurate.ConfigurationNode
import ninja.leaping.configurate.hocon.HoconConfigurationLoader
import java.io.File

import java.io.InputStream
import java.nio.file.Files


class ConfigManager(plugin: AesterPlugin) {
    private val plugin = plugin
    private var configs = hashMapOf<String, ConfigurationNode>()

    init {
        configs["aester.config"] = loadConfig("config.conf", this.javaClass.getResourceAsStream("/config.conf"))

        println(configs["aester.config"])
    }

    fun getConfig(id: String): ConfigurationNode {
        return configs[id] ?: HoconConfigurationLoader.builder().build().createEmptyNode()
    }

    fun loadConfig(fileName: String, default: InputStream): ConfigurationNode {
        var file = File(plugin.dataFolder, fileName)
        if (!file.exists()) {
            saveDefault(default, fileName)
        }

        return HoconConfigurationLoader.builder().setPath(file.toPath()).build().load()
    }

    fun loadConfig(folder: String, fileName: String, default: InputStream): ConfigurationNode {
        var file = File(plugin.dataFolder.resolve(folder), fileName)
        if (!file.exists()) {
            saveDefault(default, folder, fileName)
        }

        return HoconConfigurationLoader.builder().setPath(file.toPath()).build().load()
    }

    fun saveDefault(inputStream: InputStream, fileName: String) {


        Files.copy(inputStream, File(plugin.dataFolder, fileName).toPath())
    }

    fun saveDefault(inputStream: InputStream, folder: String, fileName: String) {
        Files.copy(inputStream, File(plugin.dataFolder.resolve(folder), fileName).toPath())
    }

}