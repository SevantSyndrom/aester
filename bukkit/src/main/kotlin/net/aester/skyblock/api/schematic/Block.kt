package net.aester.skyblock.api.schematic

import com.google.gson.Gson
import net.aester.skyblock.api.schematic.BlockAttributeMap.AttributeType.*
import net.aester.skyblock.api.schematic.BlockAttributeMap.BlockStateType.*
import org.bukkit.Material
import org.bukkit.block.*
import org.bukkit.block.Block
import org.bukkit.material.FlowerPot

class Block(block: Block) {

    val attributeMap = BlockAttributeMap()
    val gson = Gson()

    init {

        when (block.type) {

            Material.BEACON -> {
                val b = block as Beacon
                attributeMap[POTIONEFFECT] = BlockDataAttribute(b.primaryEffect.toString())
                val items = hashMapOf<Int, String>()
                for (i in 0..b.inventory.contents.size) {
                    items[i] = gson.toJson(b.inventory.contents[i].serialize())
                }
                attributeMap[INVENTORY] = BlockDataAttribute(items)
                attributeMap[STATETYPE] = BlockDataAttribute(BEACON.toString())

            }
            Material.BREWING_STAND -> {
                var b = block as BrewingStand
                attributeMap[BREWINGTIME] = BlockDataAttribute(b.brewingTime)
                attributeMap[FUELLEVEL] = BlockDataAttribute(b.fuelLevel)
                attributeMap[STATETYPE] = BlockDataAttribute(BREWINGSTAND.toString())
            }

            Material.FURNACE -> {
                val b = block as Furnace
                attributeMap[BURNTIME] = BlockDataAttribute(b.burnTime)
                attributeMap[COOKTIME] = BlockDataAttribute(b.cookTime)
                val items = hashMapOf<Int, String>()
                for (i in 0..b.inventory.contents.size) items[i] = gson.toJson(b.inventory.contents[i].serialize())
                attributeMap[INVENTORY] = BlockDataAttribute(items)
                attributeMap[STATETYPE] = BlockDataAttribute(FURNACE)
            }

            Material.CHEST -> {
                val b = block as Chest
                val items = hashMapOf<Int, String>()
                for (i in 0..b.inventory.contents.size) items[i] = gson.toJson(b.inventory.contents[i].serialize())
                attributeMap[INVENTORY] = BlockDataAttribute(items)
                attributeMap[STATETYPE] = BlockDataAttribute(CHEST.toString())
            }

            Material.DISPENSER -> {
                val b = block as Dispenser
                val items = hashMapOf<Int, String>()
                for (i in 0..b.inventory.contents.size) items[i] = gson.toJson(b.inventory.contents[i].serialize())
                attributeMap[INVENTORY] = BlockDataAttribute(items)
                attributeMap[STATETYPE] = BlockDataAttribute(DISPENSER.toString())
            }

            Material.DROPPER -> {
                val b = block as Dropper
                val items = hashMapOf<Int, String>()
                for (i in 0..b.inventory.contents.size) items[i] = gson.toJson(b.inventory.contents[i].serialize())
                attributeMap[INVENTORY] = BlockDataAttribute(items)
                attributeMap[STATETYPE] = BlockDataAttribute(DROPPER.toString())
            }

            Material.HOPPER -> {
                val b = block as Hopper
                val items = hashMapOf<Int, String>()
                for (i in 0..b.inventory.contents.size) items[i] = gson.toJson(b.inventory.contents[i].serialize())
                attributeMap[INVENTORY] = BlockDataAttribute(items)
                attributeMap[STATETYPE] = BlockDataAttribute(HOPPER.toString())
            }

            Material.COMMAND_BLOCK -> {
                val b = block as CommandBlock
                attributeMap[COMMAND] = BlockDataAttribute(b.command)
                attributeMap[COMMANDBLOCKNAME] = BlockDataAttribute(b.name)
                attributeMap[STATETYPE] = BlockDataAttribute(COMMANDBLOCK.toString())
            }

            Material.SPAWNER -> {
                val b = block as CreatureSpawner
                attributeMap[DELAY] = BlockDataAttribute(b.delay)
                attributeMap[STATETYPE] = BlockDataAttribute(SPAWNER.toString())
            }

            Material.JUKEBOX -> {
                attributeMap[PLAYING] = BlockDataAttribute((block as Jukebox).isPlaying)
            }

            Material.END_GATEWAY -> {
                val b = block as EndGateway
                attributeMap[EXACTTP] = BlockDataAttribute(b.isExactTeleport)
                val loc = b.exitLocation
                attributeMap[EXITLOCATION] = BlockDataAttribute("${loc.x}:${loc.y}:${loc.z}:${loc.world.name}")
                attributeMap[STATETYPE] = BlockDataAttribute(ENDGATE.toString())

            }
            Material.SHULKER_BOX -> {
                val b = block as ShulkerBox
                val items = hashMapOf<Int, String>()
                for (i in 0..b.inventory.contents.size) items[i] = gson.toJson(b.inventory.contents[i].serialize())
                attributeMap[INVENTORY] = BlockDataAttribute(items)
                attributeMap[STATETYPE] = BlockDataAttribute(SHULKERBOX.toString())
            }

            Material.FLOWER_POT -> {
                val b = block.blockData as FlowerPot
                attributeMap[FLOWER] = BlockDataAttribute("${b.contents.itemType}:${b.contents.data}")
                attributeMap[STATETYPE] = BlockDataAttribute(FLOWERPOT.toString())
            }


        }

    }
}