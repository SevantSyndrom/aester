package net.aester.skyblock.api.schematic

class BlockAttributeMap : AttributeMap<BlockAttributeMap.AttributeType, BlockDataAttribute<*>>() {

    operator fun get(type: AttributeType): BlockDataAttribute<*>? {
        return map[type]
    }

    operator fun set(key: AttributeType, value: BlockDataAttribute<*>) {
        map[key] = value
    }

    enum class AttributeType {
        MATERIAL,
        BLOCKDATA,
        BIOME,
        STATETYPE,
        DATATYPE,
        BASECOLOR,
        POTIONEFFECT,
        COMMAND,
        COMMANDBLOCKNAME,
        ENTITY,
        EXITLOCATION,
        FLOWER,
        PLAYING,
        SIGNLINES,
        ROTATEFACE,
        SKULLOWNER,
        SKULLTYPE,
        FACING,
        INVENTORY,
        VERSION,
        POSX,
        POSY,
        POSZ,
        BREWINGTIME,
        FUELLEVEL,
        DELAY,
        DATA,
        BURNTIME,
        COOKTIME,
        PATTERNS,
        EXACTTP

    }

    enum class BlockStateType {
        NORMAL, BANNER, BEACON, BREWINGSTAND, COMMANDBLOCK, CHEST, DISPENSER, DROPPER, HOPPER, SHULKERBOX, SPAWNER, ENDGATE, FURNACE, JUKEBOX, SIGN, SKULL, DOUBLECHEST, FLOWERPOT
    }
}